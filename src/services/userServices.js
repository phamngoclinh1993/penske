export const getUserList = (conditions, limit, offset) => {
  let url = '../data.json';
  let data = {
    offset: offset,
    limit: limit
  };

  if (conditions && conditions !== null) {
    data = Object.assign(conditions, data);
  }

  // let option = {
  //     method: 'POST', // or 'PUT'
  //     body: JSON.stringify(data),
  //     headers: new Headers({
  //       'Content-Type': 'application/json'
  //     })
  // };

  return fetch(url, {qs: data});
};