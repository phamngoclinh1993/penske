import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import EmployeesFilters from "./containers/EmployeesFilters";

class App extends Component {
  render() {
    return (
      <div className="App">
        <main>
          <EmployeesFilters/>
        </main>
      </div>
    );
  }
}

export default App;
