import * as userServices from '../services/userServices';

export const REQUEST_USER_LIST = 'REQUEST_USER_LIST';
export const REQUEST_USER_LIST_FAIL = 'REQUEST_USER_LIST_FAIL';
export const REQUEST_USER_LIST_SUCCESS = 'REQUEST_USER_LIST_SUCCESS';

export const requestUserList = (conditions, offset, limit) => {
  return {
    type: REQUEST_USER_LIST,
    offset,
    limit
  }
};

export const requestUserListFail = (error) => {
  return {
    type: REQUEST_USER_LIST_FAIL,
    error: error
  }
};

export const requestUserListSuccess = (response) => {
  return {
    type: REQUEST_USER_LIST_SUCCESS,
    response: response
  }
};

export const getUserList = (conditions, offset, limit) => {
  return dispatch => {
    dispatch(requestUserList(conditions, offset, limit));
    return userServices.getUserList(offset, limit);
  }
};