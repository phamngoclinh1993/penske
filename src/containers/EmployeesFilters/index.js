import {connect} from 'react-redux';
import EmployeesFilters from './EmployeesFilters';

import * as userActions from '../../actions/userActions';

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
  return {
    user: state.auth
  }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    getUserList: (conditions, offset, limit) => dispatch(userActions.getUserList(conditions, offset, limit)),
    getUserListFail: (error) => dispatch(userActions.requestUserListFail(error)),
    getUserListSuccess: (response) => dispatch(userActions.requestUserListSuccess(response))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(EmployeesFilters);