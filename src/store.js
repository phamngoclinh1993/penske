import {createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import createBrowserHistory from 'history/createBrowserHistory';

import rootReducer from './reducers/rootReducer';

// defaultStore
const initStore = {};

const loggerMiddleware = createLogger();

// create store
const store = createStore(rootReducer, initStore, applyMiddleware(
	thunkMiddleware,
	loggerMiddleware
));

console.log(store.getState());

// create history
const customHistory = createBrowserHistory();

export const history = customHistory;

export default store;

