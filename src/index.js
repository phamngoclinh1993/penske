import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import {Router, Route, Switch} from 'react-router';
import {Provider} from 'react-redux';
import store, {history} from './store';

import registerServiceWorker from './registerServiceWorker';

const router = (
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact path='/' component={App}/>
        <Route path="/" component={App}/>
        {/* <Route component={NotFound}/> */}
      </Switch>
    </Router>
  </Provider>
);

ReactDOM.render(router, document.getElementById('root'));

registerServiceWorker();
