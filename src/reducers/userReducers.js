import {
  REQUEST_USER_LIST, REQUEST_USER_LIST_FAIL, REQUEST_USER_LIST_SUCCESS
} from '../actions/userActions';

const users = (state = {
                 isFetching: false,
                 total: 0,
                 limit: 100,
                 offset: 0,
                 data: []
               },
               action) => {
  switch (action.type) {

    case REQUEST_USER_LIST:
      return Object.assign({}, state, {
        isFetching: true,
        limit: action.limit,
        offset: action.offset
      });

    case REQUEST_USER_LIST_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        response: action.response,
        data: action.response.data
      });

    case REQUEST_USER_LIST_FAIL:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error,
      });

    default:
      return state;
  }
};

export default users;